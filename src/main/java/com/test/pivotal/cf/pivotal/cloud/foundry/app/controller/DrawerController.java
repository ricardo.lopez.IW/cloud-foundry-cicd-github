package com.test.pivotal.cf.pivotal.cloud.foundry.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/info")
public class DrawerController {

  @GetMapping
  public String information() {
    return "CI/CD works!! 2";
  }

}
