package com.test.pivotal.cf.pivotal.cloud.foundry.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PivotalCloudFoundryAppApplication {

  public static void main(String[] args) {
    SpringApplication.run(PivotalCloudFoundryAppApplication.class, args);
  }
}
